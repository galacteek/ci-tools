ci-tools
========

yaml-xform
----------

Transforms a YAML file using OmegaConf resolvers. Supported resolvers:

- *yaml_get*: Pulls a YAML file from the given http URL.

```yaml
something: ${yaml_get:https://localhost/files/file1.yaml}
```

- *dt_now*: Current datetime.

```yaml
date: ${dt_now}
```

Usage
^^^^^

Usage (output to a file):

```sh
yaml-xform -s template.yaml -o output.yaml
```

Usage (output to stdout):

```sh
yaml-xform -s template.yaml
```
