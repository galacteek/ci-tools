import sys
import argparse
import traceback
import requests
import asyncio

from datetime import datetime
from omegaconf import OmegaConf
from omegaconf.dictconfig import DictConfig


def yaml_get(yaml_url: str):
    """
    Pulls the YAML content from the given URL.
    """

    try:
        data = requests.request('GET', yaml_url)

        if data.status_code != 200:
            print(f'GET {yaml_url} failed', file=sys.stderr)

            raise Exception(f'{yaml_url}: HTTP code: {data.status_code}')

        if data.content:
            obj = OmegaConf.create(data.content.decode())

            return OmegaConf.to_container(obj, resolve=True)
    except Exception as err:
        print(f'Failed to read: {yaml_url}: {err}', file=sys.stderr)


OmegaConf.register_new_resolver("yaml_get", yaml_get)
OmegaConf.register_new_resolver("dt_now",
                                lambda x: datetime.now().isoformat())


def xform_run(source, dest):
    try:
        if isinstance(source, str):
            oc = OmegaConf.load(source)
        elif isinstance(source, DictConfig):
            oc = source

        c = OmegaConf.to_container(oc, resolve=True)

        if isinstance(dest, str):
            if dest == '-':
                OmegaConf.save(c, sys.stdout)
            else:
                OmegaConf.save(c, dest)

        return OmegaConf.create(c)
    except Exception:
        traceback.print_exc()


async def xform_run_async(source, dest=None, loop=None, timeout=30):
    loop = loop if loop else asyncio.get_event_loop()

    return await asyncio.wait_for(
        loop.run_in_executor(None, xform_run, source, dest),
        timeout
    )


def xform_main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--src', '-s', dest='tmpl')
    parser.add_argument('--out', '-o', dest='dest', default='-')
    args = parser.parse_args()

    if not args.tmpl:
        parser.print_help(sys.stderr)
        sys.exit(1)

    if xform_run(args.tmpl, args.dest) is not None:
        sys.exit(0)
    else:
        sys.exit(1)
