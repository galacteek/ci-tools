from setuptools import setup
from setuptools import find_packages


with open('README.md', 'r') as fh:
    long_description = fh.read()

found_packages = find_packages(exclude=['tests', 'tests.*'])

setup(
    name='ci-tools',
    version='1.0.1',
    license='GPL3',
    author='cipres',
    url='https://gitlab.com/galacteek/ci-tools',
    description='CI tools',
    long_description=long_description,
    packages=found_packages,
    install_requires=[
        'omegaconf==2.1.1',
        'requests'
    ],
    entry_points={
        'console_scripts': [
            'yaml-xform = galacteek_ci_tools.entrypoint:xform'
        ],
    }
)
