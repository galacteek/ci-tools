image: "python:3.8"

vars:
  stage: vars
  script:
    - VERSION="1.0.1"
    - WHEEL_NAME=ci_tools
    - WHEEL_FILENAME="${WHEEL_NAME}-${VERSION}-py3-none-any.whl"

    - echo "VERSION=$VERSION" > vars.env

    - >
      echo "WHEEL_NAME=${WHEEL_NAME}"
      >> vars.env

    - >
      echo "WHEEL_FILENAME=${WHEEL_FILENAME}"
      >> vars.env

    - >
      echo "WHEEL_ARTIFACT_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${VERSION}/${WHEEL_FILENAME}"
      >> vars.env

    - >
      echo "WHEEL_DOWNLOAD_URL=${CI_PROJECT_URL}/-/releases/${VERSION}/downloads/${WHEEL_FILENAME}"
      >> vars.env

    - >
      echo "DISTMAP_ASSET_URL=${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/continuous-${CI_COMMIT_BRANCH}/dist-wheel.yaml"
      >> vars.env

  artifacts:
    reports:
      dotenv: vars.env

stages:
  - vars
  - build
  - release

build:
  stage: build
  script:
    - python3 -m pip install --upgrade pip
    - python3 -m venv venv

    - source venv/bin/activate
    - pip install wheel
    - pip install -e '.'

    - python setup.py sdist bdist_wheel

    # Compute the python wheel's sha512 checksum
    - export WHEEL_SHA512_CHECKSUM=$(sha512sum "dist/${WHEEL_FILENAME}"|awk '{print $1}')
    - export WHEEL_SHA256_CHECKSUM=$(sha256sum "dist/${WHEEL_FILENAME}"|awk '{print $1}')

    # Build the yaml wheel dist infos file with yaml-xform
    - yaml-xform -s .gitlab/wheel.tmpl.yaml -o dist-wheel.yaml

  artifacts:
    expire_in: 'never'
    paths:
      - dist/*.whl
      - dist-wheel.yaml

release:
  image: registry.gitlab.com/gitlab-org/release-cli
  stage: release
  script:
    - apk add bash curl
    - TAGNAME=${VERSION}
    - WHEEL_SHA256_CHECKSUM=$(sha256sum "dist/${WHEEL_FILENAME}"|awk '{print $1}')

    - PIP_REQUIREMENT_STATEMENT="${WHEEL_NAME} @ ${WHEEL_DOWNLOAD_URL}#sha256=${WHEEL_SHA256_CHECKSUM}"
    - echo "${PIP_REQUIREMENT_STATEMENT}" > pip_req

    - >
      curl
      --request DELETE
      --header "JOB-TOKEN: $CI_JOB_TOKEN"
      --header "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}"
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/releases/${TAGNAME}"

    - >
     curl --header "JOB-TOKEN: $CI_JOB_TOKEN"
     --upload-file
     dist/${WHEEL_FILENAME}
     ${WHEEL_ARTIFACT_URL}

    - >
      curl --progress-bar
      --header "JOB-TOKEN: $CI_JOB_TOKEN"
      --upload-file dist-wheel.yaml ${DISTMAP_ASSET_URL}

    - >
      release-cli create
      --name release-${TAGNAME}
      --description pip_req
      --tag-name $TAGNAME
      --ref $CI_COMMIT_SHA
      --assets-link
      "{\"name\": \"${WHEEL_FILENAME}\", \"filepath\": \"/${WHEEL_FILENAME}\", \"url\": \"${WHEEL_ARTIFACT_URL}\"}"
      --assets-link
      "{\"name\" :\"dist-wheel.yaml\", \"filepath\": \"/dist-wheel.yaml\", \"url\": \"${DISTMAP_ASSET_URL}\"}"
